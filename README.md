# aws-instance-metadata
This node script runs on an AWS EC2 instance, calls the EC2 API and outputs metadata in JSON format to stdout.
The data is wrapped in an object with a key of `metaData`.

(The instance has to have node and git installed. 🙂)

## Install
With a terminal open on the EC2 instance run:
```
git clone https://gabrielmccallin@bitbucket.org/gabrielmccallin/aws-instance-metadata.git
```  

In the `/aws-instance-metadata` folder created run:
```
npm i
```

## Usage
In the `/aws-instance-metadata` folder run:
```
node index.js
```

With a key:
```
node index.js ami-id
```

Available keys:
```
ami-id
ami-launch-index
ami-manifest-path
block-device-mapping/
hostname
instance-action
instance-id
instance-type
local-hostname
local-ipv4
mac
metrics/
network/
placement/
profile
public-hostname
public-ipv4
public-keys/
reservation-id
security-groups
services/
```
Nested information e.g. `services/` will return as a string. This is a limitation of the current version.
