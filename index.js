var AWS = require('aws-sdk');
var meta = new AWS.MetadataService();

const getMeta = (key) => {
    return new Promise((resolve, reject) => {
        meta.request(`/latest/meta-data/${key}`, (err, data) => {
            (err) ? reject(err) : resolve(data);
        });
    });
}

const getMetaKey = (key) => {
    return new Promise((resolve, reject) => {
        return getMeta(key).then(data => resolve({ [key]: data }))
            .catch(error => reject(error));
    });
}

const getAll = () => {
    getMeta('').then(toplevel => toplevel)
        .then(list => {
            const array = list.split('\n');
            const promises = array.map(item => getMetaKey(item));
            Promise.all(promises).then(values => process.stdout.write(JSON.stringify({ metaData: values }) + '\n'));
        });
}

const key = process.argv[2] ? process.argv[2] : '';

(key === '') ? getAll() : getMetaKey(key)
    .then(values => process.stdout.write(JSON.stringify({ metaData: values }) + '\n'))
    .catch(error => process.stderr.write('ERROR: not a key!' + '\n'));
